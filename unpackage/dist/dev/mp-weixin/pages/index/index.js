"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      title: "我的毕设",
      picture: [
        { id: "1", url: "/static/uploads/goods_big_1.jpg" },
        { id: "2", url: "/static/uploads/goods_big_2.jpg" },
        { id: "3", url: "/static/uploads/goods_big_3.jpg" },
        { id: "4", url: "/static/uploads/goods_big_4.jpg" }
      ]
    };
  },
  onLoad() {
  },
  methods: {
    onPreviewImg(url) {
      common_vendor.wx$1.previewImage({
        urls: this.picture.map((v) => v.url),
        current: url
      });
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.f($data.picture, (item, k0, i0) => {
      return {
        a: item.url,
        b: common_vendor.o(($event) => $options.onPreviewImg(item.url), item.id),
        c: item.id
      };
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "D:/Otherfile/workplace/uni-app-project/pages/index/index.vue"]]);
wx.createPage(MiniProgramPage);
